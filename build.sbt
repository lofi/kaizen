
name := """kaizen"""

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.1"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SbtWeb)

libraryDependencies ++= Seq(
    jdbc,
    anorm,
    cache,
    filters,
    ws,
    "com.typesafe.slick" %% "slick" % "2.1.0-RC1",
    "com.typesafe.play" %% "play-slick" % "0.8.0-M1"
  )

