# Application codename Kaizen

Pretty standard Play deploy and build setup. Do the thing.

Builds should be automatically triggered when you push. A bitbucket user was created in jenkins and a token was created with uuidgen and associated with the project and the jenkins hook on the bitbucket project.

## Rough Milestones

Here are just a couple goal posts with arbitrary version assignment. This will certainly change but for now it’s about as close to planning as we can get until our first milestone.

- first post (pastes with comment threads) < version series 0.1.0 >
- attachments (attach/upload a set of files instead of working on a single paste.) < version series 0.2.0 >
- vcs integration (pull diffs or files from a repository) < version series 0.3.0 >

### Details

#### First post

*todo*

#### Attachments

*todo*

#### VCS Integration

*todo*

