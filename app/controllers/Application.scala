package controllers

import play.api.data._
import play.filters.csrf._
import play.api.db.slick.DBAction
import play.api.mvc._
import utils.Base62
import scala.slick.driver.H2Driver.simple._

import play.api.data.Forms._

case class PasteData(title: Option[String], body: String)

object Application extends Controller {
  val pastes = TableQuery[models.Pastes]

  val pasteForm = Form(
    mapping(
      "title" -> optional(text),
      "body" -> nonEmptyText
    )(PasteData.apply)(PasteData.unapply)
  )

  def pasteList = DBAction { implicit rs =>
    Ok(views.html.paste_list(pastes.run(rs.dbSession)))
  }

  def pasteEdit = CSRFAddToken {
    Action { implicit request =>
      Ok(views.html.paste_edit(pasteForm))
    }
  }

  def pasteDetail(encodedId: String) = DBAction { rs =>
    pastes.filter(_.id === Base62.decode(encodedId)).firstOption(rs.dbSession) match {
      case None => NotFound("Paste Not Found")
      case Some(paste) => Ok(views.html.paste_detail(paste))
    }
  }

  def pasteUpload = CSRFCheck {
    DBAction { implicit rs =>
      pasteForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(views.html.paste_edit(formWithErrors))
        },
        pasteData => {
          rs.dbSession.withTransaction {
            implicit val session = rs.dbSession
            val pasteId =
              pastes.map(p => (p.title.?, p.body)) returning pastes.map(_.id) += (pasteData.title, pasteData.body)
            Redirect(routes.Application.pasteDetail(Base62.encode(pasteId)))
          }
        }
      )
    }
  }

}
