package models

import java.sql.Timestamp
import controllers.routes
import utils.Base62
import scala.slick.driver.H2Driver.simple._


case class Paste(id: Int,
                 title: Option[String],
                 body: String,
                 created: Timestamp,
                 updated: Timestamp) {



  def detailUrl = routes.Application.pasteDetail(Base62.encode(id))
}

class Pastes(tag: Tag) extends Table[(Paste)](tag, "pastes") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title = column[String]("title", O.Nullable)
  def body = column[String]("body", O.DBType("CLOB"))
  def created = column[Timestamp]("created", O.DBType("TIMESTAMP DEFAULT CURRENT_TIMESTAMP"))
  def updated = column[Timestamp]("updated", O.DBType("TIMESTAMP AS CURRENT_TIMESTAMP"))

  def * = (id, title.?, body, created, updated) <> (Paste.tupled, Paste.unapply)
}
