package utils

import scala.annotation.tailrec
import scala.math.pow

object Base62 {
  private val alphabet = ('a' to 'z') ++ ('A' to 'Z') ++ (0 to 9).mkString.toList
  private val base = alphabet.size // should be 62!

  def encode(num: Int): String = {
    @tailrec
    def loop(n: Int, acc: List[Int]): String =
      if (n > 0) loop(n / base, acc ++ List(n % base))
      else acc.reverse.map(i => alphabet(i)).mkString

    loop(num, Nil)
  }

  def decode(s: String): Int = {
    @tailrec
    def loop(c: List[Char], power: Int, acc: Int = 0): Int =
      if(c.isEmpty) acc
      else loop(c.tail, power - 1, acc + alphabet.indexOf(c.head) * pow(base, power).toInt)

    loop(s.toList, s.size - 1)
  }
}
