# Bookmarks

[play-yeoman](https://github.com/tuplejump/play-yeoman)

[jGit](http://eclipse.org/jgit/)

[SVNkit](svnkit.com)

[Unboundid](https://www.unboundid.com/products/ldap-sdk/)

[Preventing LDAP injection in java](https://www.owasp.org/index.php/Preventing_LDAP_Injection_in_Java) 
*not sure how important this is of course... useful reference though*

## Gitblit dependencies

Gitblit is a modern app which certainly has some features and abilities that Kaizen will. Here are some of it's potentially useful dependencies.

- 'javax.mail:mail:1.4.3' [javadoc](http://docs.oracle.com/javaee/5/api/javax/mail/package-summary.html)
- 'org.pegdown:pegdown:1.4.2' [github repo](https://github.com/sirthias/pegdown)
- 'org.fusesource.wikitext:wikitext-core:1.4'
- 'org.fusesource.wikitext:twiki-core:1.4'
- 'org.fusesource.wikitext:textile-core:1.4'
- 'org.fusesource.wikitext:tracwiki-core:1.4'
- 'org.fusesource.wikitext:mediawiki-core:1.4'
- 'org.fusesource.wikitext:confluence-core:1.4'
- 'org.eclipse.jgit:org.eclipse.jgit:3.3.1.201403241930-r' [jGit](http://eclipse.org/jgit/)
- 'com.unboundid:unboundid-ldapsdk:2.3.0' [Unboundid](https://www.unboundid.com/products/ldap-sdk/)

## Deployment

Plan at the moment is to use OpenShift. We already have a jenkins server running: `https://jenkins-lofi.rhcloud.com/`
