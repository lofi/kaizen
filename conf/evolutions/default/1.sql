# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table "pastes" ("id" INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1) NOT NULL PRIMARY KEY,"title" VARCHAR,"body" CLOB NOT NULL,"created" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,"updated" TIMESTAMP AS CURRENT_TIMESTAMP NOT NULL);

# --- !Downs

drop table "pastes";

